import { Theme, createStyles } from '@material-ui/core'

export const usersStyles = (theme: Theme) =>
  createStyles({
    root: {},
    header: {
      marginBottom: theme.spacing(4)
    }
  })

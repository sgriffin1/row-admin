import { makeStyles, Tab } from '@material-ui/core'
import { useState } from 'react'
import { TabList, TabPanel, TabContext } from '@material-ui/lab'
import { settingsStyles } from './Settings@styles'
import { ChangeEvent } from 'react'
import { useTranslation } from 'react-i18next'
import {
  GeneralSettings,
  ProfileSettings,
  NotificationsSettings,
} from './Settings@parts'

const useStyles = makeStyles(settingsStyles)

const Settings = () => {
  // state
  const [value, setValue] = useState<string>('general')

  const handleChangeTab = (event: ChangeEvent<{}>, newValue: string) => {
    setValue(newValue)
  }

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  const tabsList = [
    {
      label: t`Settings.General`,
      value: 'general',
    },
    {
      label: t`Settings.Profile`,
      value: 'profile',
    },
    {
      label: t`Settings.Notifications`,
      value: 'notifications',
    },
  ]

  const tabPanels = [
    {
      value: 'general',
      content: <GeneralSettings />,
    },
    {
      value: 'profile',
      content: <ProfileSettings />,
    },
    {
      value: 'notifications',
      content: <NotificationsSettings />,
    },
  ]

  return (
    <div className={classes.root}>
      <TabContext value={value}>
        <TabList onChange={handleChangeTab} textColor="primary" indicatorColor="primary" aria-label="simple tabs example">
          {tabsList.map((tab, index) => (
            <Tab key={index} label={tab.label} value={tab.value} />
          ))}
        </TabList>

        {tabPanels.map((tabPanel, index) => (
          <TabPanel
            color="primary"
            key={index}
            value={tabPanel.value}
            className={classes.tabPanel}
          >
            {tabPanel.content}
          </TabPanel>
        ))}
      </TabContext>
    </div>
  )
}

export default Settings

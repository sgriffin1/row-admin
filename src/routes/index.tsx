import { useContext } from 'react'
import { Switch, Route } from 'react-router-dom'

import { Layout } from '../components/Layout'
import { RouteName } from '../enums/RouteName'
import { AuthContext } from '../providers/auth.provider'

// ROUTES
/* ROOT */
import { Dashboard } from './dashboard'
import { HelpCenter } from './help-center'
/* MANAGEMENT */
import { Users } from './users'
import { Contacts } from './contacts'
import { Tasks } from './tasks'
/* WEB MANAGEMENT */
import { Orders } from './orders'
import { Domains } from './domains'
import { Questions } from './questions'
/* GASTRONOMY */
import { DailyMenu } from './daily-menu'
import { FoodMenu } from './food-menu'
import { OpeningHours } from './opening-hours'
/* APP */
import { NotFound } from './not-found'
import { Login } from './login'
import { About } from './about'
import { UserProfile } from './user-profile'
import { Settings } from './settings'

const Routing = () => {
  const { userId } = useContext(AuthContext)

  return (
    <Switch>
      {!userId ? (
        <Login />
      ) : (
        <Layout>
          <Switch>
            {/* ROOT */}
            <Route path={RouteName.Dashboard} exact component={Dashboard} />
            <Route path={RouteName.HelpCenter} exact component={HelpCenter} />

            {/* MANAGEMENT */}
            <Route path={RouteName.Contacts} exact component={Contacts} />
            <Route path={RouteName.Tasks} exact component={Tasks} />
            <Route path={RouteName.Users} exact component={Users} />

            {/* WEB MANAGEMENT */}
            <Route path={RouteName.Orders} exact component={Orders} />
            <Route path={RouteName.Domains} exact component={Domains} />
            <Route path={RouteName.Question} exact component={Questions} />

            {/* GASTRONOMY */}
            <Route path={RouteName.DailyMenu} exact component={DailyMenu} />
            <Route path={RouteName.FoodMenu} exact component={FoodMenu} />
            <Route
              path={RouteName.OpeningHours}
              exact
              component={OpeningHours}
            />

            {/* APPS */}
            <Route path={RouteName.About} exact component={About} />

            {/* USER */}
            <Route path={RouteName.UserProfile} exact component={UserProfile} />
            <Route path={RouteName.Settings} exact component={Settings} />

            <Route component={NotFound} />
          </Switch>
        </Layout>
      )}
    </Switch>
  )
}

export default Routing

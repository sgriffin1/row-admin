import { makeStyles } from '@material-ui/styles'
import { useContext, useEffect, useState } from 'react'
import AddIcon from '@material-ui/icons/Add'
import { useTranslation } from 'react-i18next'
import { AlertsContext } from '../../providers/alerts.provider'
import { DataContext } from '../../providers/data.provider'
import {
  IDailyMenuInfo,
  IDailyMenuSoup,
  IDailyMenu,
} from '../../types/daily-menu.types'
import { partsStyles } from './DailyMenu@styles'
import { DailyMenuItemPropsTypes } from './DailyMenu@types'
import { dailyMenuData, dailyMenuSoupData } from './DailyMenu@config'
import guid from '../../utils/guid'
import { AlertTypes } from '../../types/alerts.types'
import { getTodayInSystemFormat } from '../../utils/datetime'
import {
  Button,
  Grid,
  TextField,
  Typography,
  FormControlLabel,
  Switch,
} from '@material-ui/core'
import { Paper } from '@material-ui/core'
import MenuSettings from './MenuSettings'
import DummySkeleton from './DummySkeleton'
import Soup from './Soup'
import { SubCollections } from '../../remote/Collections'

const useStyles = makeStyles(partsStyles)

const DailyMenuItems = (props: DailyMenuItemPropsTypes) => {
  // state
  const [loading, setLoading] = useState<boolean>(false) // TODO turn on
  const [dailyMenuDay, setDailyMenuDay] = useState<IDailyMenuInfo>({
    menuDate: '',
    description: '',
    isCanceled: false,
    reasonForCancel: '',
    soups: [],
    menu: [],
  })

  // props
  const { collection, documents } = props

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()
  const {
    insertField,
    fieldData,
    getFiledData,
    get,
    data,
    insert,
    update,
    remove,
  } = useContext(DataContext)
  const { setAlert } = useContext(AlertsContext)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDailyMenuDay({
      ...dailyMenuDay,
      [event.target.name]: event.target.value,
    })
  }

  const handleSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDailyMenuDay({
      ...dailyMenuDay,
      [event.target.name]: event.target.checked,
    })
  }

  // Soup actions
  const handleAddNewSoup = () => {
    insert({
      collection,
      documents,
      subCollection: SubCollections.Soups,
      data: dailyMenuSoupData,
    })
  }

  const handleRemoveSoup = (id: string) => {
    remove({ collection, documents, subCollection: SubCollections.Soups, id })
  }

  const handleUpdateSoap = (id: string, data: IDailyMenuSoup) => {
    update({
      collection,
      documents,
      subCollection: SubCollections.Soups,
      id,
      data,
    })
  }

  // Menu actions
  const handleAddNewMenu = () => {
    insert({ collection, documents, data: dailyMenuData })
  }

  const handleRemoveMenu = (id: string) => {
    remove({ collection, documents, id })
  }

  const handleUpdateMenu = (id: string, data: IDailyMenu, index: number) => {
    update({ collection, documents, id, data })
    setAlert({
      id: guid(),
      title: t`Daily_Menu.Success_Alert_Title`,
      description: `Menu ${index} ${t`Daily_Menu.Success_Alert_Description_Menu`}`,
      type: AlertTypes.success,
    })
  }

  // get data
  useEffect(() => {
    getFiledData({ collection, documents, subCollection: SubCollections.Soups })
    get({ collection, documents })
  }, [])

  if (loading) {
    return <DummySkeleton />
  }

  return (
    <div>
      <Grid container spacing={2} alignItems="stretch">
        <Grid item md={3} xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Daily_Menu.Menu_Date`}
            </Typography>
            <div className={classes.spacing}>
              <TextField
                id="date"
                name="menuDate"
                label={t`Daily_Menu.Menu_Date`}
                value={dailyMenuDay.menuDate}
                type="date"
                onChange={handleChange}
                fullWidth
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </Paper>
        </Grid>

        <Grid item md={9} xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Daily_Menu.Settings`}
            </Typography>
            <div className={classes.spacing}>
              <Grid container spacing={1}>
                <Grid item md={4} xs={12} className={classes.center}>
                  <FormControlLabel
                    control={
                      <Switch
                        onChange={handleSwitch}
                        checked={dailyMenuDay.isCanceled}
                        color="secondary"
                      />
                    }
                    label={t`Daily_Menu.Cancel_Menu_For_This_Day`}
                    name="hasDayCanceled"
                  />
                </Grid>
                <Grid item md={8} xs={12}>
                  <TextField
                    id="daily-menu-reason"
                    name="reasonForCancel"
                    onChange={handleChange}
                    label={t`Daily_Menu.Reason_To_Cancel_Menu_For_This_Day`}
                    variant="outlined"
                    disabled={!dailyMenuDay.isCanceled}
                    fullWidth
                  />
                </Grid>
              </Grid>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={2}>
            {fieldData && fieldData.map((fd: IDailyMenuSoup, index: number) => (
              <Grid item xs={12} key={fd.id}>
                <Soup
                  index={index}
                  data={fd}
                  handleRemove={handleRemoveSoup}
                  handleUpdate={handleUpdateSoap}
                />
              </Grid>
            ))}

            <Grid item xs={12}>
              <Button
                variant="text"
                color="primary"
                size="large"
                onClick={handleAddNewSoup}
                fullWidth
                startIcon={<AddIcon />}
              >
                {t('Daily_Menu.Add_Soup')}
              </Button>
            </Grid>

            {!dailyMenuDay.isCanceled &&
              data.map((menu: any, index) => (
                <MenuSettings
                  key={menu.id}
                  handleRemove={handleRemoveMenu}
                  handleUpdate={handleUpdateMenu}
                  data={menu}
                  index={index}
                />
              ))}
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Button
            variant="text"
            color="primary"
            size="large"
            fullWidth
            disabled={dailyMenuDay.isCanceled}
            onClick={handleAddNewMenu}
            startIcon={<AddIcon />}
          >
            {t`Daily_Menu.Add_Menu`}
          </Button>
        </Grid>
      </Grid>
    </div>
  )
}

export default DailyMenuItems

import { IDailyMenu, IDailyMenuSoup} from '../../types/daily-menu.types'

export const dailyMenuData: IDailyMenu = {
  name: '',
  allergens: [],
  amount: 0,
  price: 0,
  isBusiness: false,
}

export const dailyMenuSoupData: IDailyMenuSoup = {
  name: '',
  amount: 0,
  price: 0,
  allergens: []
}

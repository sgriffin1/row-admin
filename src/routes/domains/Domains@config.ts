import t from 'i18next'
import { DomainsModel } from '../../models/domains.model'
import { initYup } from '../../plugins/yup'
import i18n from '../../services/i18n'
import {
  getTodayInSystemFormat
} from '../../utils/datetime'

export const createFormInsertDomainSchema = () => {
  const yup = initYup(i18n.language)

  return yup.object({
    domain: yup
      .string()
      .required()
      .label(t.t`Domains.Domain`),
    operator: yup
      .string()
      .required()
      .label(t.t`Domains.Operator`),
    phone: yup
      .string()
      .required()
      .label(t.t`Domains.Phone`),
    email: yup
      .string()
      .email()
      .required()
      .label(t.t`Domains.Email`),
    hosted: yup.string().label(t.t`Domains.Hosted`),
    expiration: yup.string().label(t.t`Domains.Expiration`),
  })
}

export const formInsertDomainInitials: DomainsModel = {
  domain: '',
  operator: '',
  phone: '',
  email: '',
  hosted: '',
  isPaid: false,
  expiration: getTodayInSystemFormat().date,
}

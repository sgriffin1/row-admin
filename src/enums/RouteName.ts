export enum RouteName {
  // Root
  Dashboard = '/',
  HelpCenter = '/help-center',

  // Management
  Users = '/users',
  Contacts = '/contacts',
  Tasks = '/tasks',

  // Web management
  Orders = '/orders',
  Domains = '/domains',
  Question = '/questions',

  // Gastronomy
  DailyMenu = '/daily-menu',
  FoodMenu = '/food-menu',
  OpeningHours = '/opening-hours',

  // App
  About = '/about',

  // Auth
  Login = '/login',

  // User
  UserProfile = '/user-profile',
  Settings = '/settings',
}

export type DomainsModel = {
  id?: string
  domain: string
  operator: string
  phone: string
  email: string
  hosted: string
  isPaid: boolean
  expiration: string
}

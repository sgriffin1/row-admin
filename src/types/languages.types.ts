export type LanguageTypes = {
  key: string
  label: string
  value: string
  flag: string
}
export enum Collections {
  WebManagement = 'web-management',
  Management = 'managemennt',
  Daily_Menu = 'daily-menu',
}

export enum Documents {
  Orders = 'orders',
  Tasks = 'tasks',
  Domains = 'domains',
  Contacts = 'contacts',
  Questions = 'questions',
  // Daily menu
  Monday = 'monday',
  Tuesday = 'tuesday',
  Wednesday = 'wednesday',
  Thursday = 'thursday',
  Friday = 'friday',
  Saturday = 'saturday',
  Sunday = 'sunday',
}

export enum SubCollections {
  Data = 'data',

  // DAILY MENU
  Soups = 'soups',
}

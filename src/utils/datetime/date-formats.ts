const dateFormats = {
  formatDateLocal: 'DD. MM. YYYY',
  formatDateGlobal: 'MM/DD/YYYY',
  formatDateIso: 'YYYY-MM-DD',

  formatTimeLocal: 'HH:MM'
}

export default dateFormats
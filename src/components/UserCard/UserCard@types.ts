import { User } from '../../types/user.types'

export interface UserCardPropsTypes extends User {}

export interface UserCardStylesPropsTypes {
  coverUrl?: string
}

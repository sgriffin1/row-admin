import { AlertTypes } from '../../types/alerts.types'

export const alerts = [
  {
    title: 'Success',
    description: 'Some description',
    type: AlertTypes.success,
  },
  {
    title: 'Warning',
    description: 'Some description',
    type: AlertTypes.warning,
  },
  {
    title: 'Error',
    description: 'Some description',
    type: AlertTypes.error,
  },
  {
    title: 'Info',
    description: 'Some description',
    type: AlertTypes.info,
  },
]

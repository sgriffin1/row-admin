import { Typography, Tooltip } from '@material-ui/core'
import { classnames } from '@material-ui/data-grid'
import { makeStyles } from '@material-ui/styles'
import { useState } from 'react'
import { tagStyles } from './Tag@styles'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(tagStyles)

interface ITagProps {
  identifier?: string
  label: string
  isActive?: boolean
  tooltip?: string
  onClick?: (id: string) => void
}

const Tag = (props: ITagProps) => {
  // props
  const { identifier, label, tooltip, onClick, isActive = false } = props

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  // reaction
  const behavior = isActive
    ? classnames([classes.root, classes.active])
    : classes.root

  // action
  const handleAction = (): void => {
    if (onClick && identifier) {
      return onClick(identifier)
    } else {
      return console.log('Not yet set' + identifier)
    }
  }

  return (
    <>
      {tooltip ? (
        <Tooltip title={tooltip}>
          <button className={behavior} onClick={handleAction}>
            <Typography variant="caption" display="block">
              {label}
            </Typography>
          </button>
        </Tooltip>
      ) : (
        <button className={behavior} onClick={handleAction}>
          <Typography variant="caption" display="block">
            {label}
          </Typography>
        </button>
      )}
    </>
  )
}

export default Tag
